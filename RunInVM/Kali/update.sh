
#! /bin/env bash
#IMPORTANT: DONOT call with `.` use `bash` instead.
#Stop on error
	set -euo pipefail
#Make the vars either set or empty string
UPDATE_ATTACK=${UPDATE_ATTACK:-}
UPDATE_DEV=${UPDATE_DEV:-}

echo "-------VM Update--------"
if [ -z "$UPDATE_DEV" ] && [ -z ${NO_UPDATE_DEV+x} ]; then  
    echo "Update Dev? [1/2]"
    select yn in "Yes" "No"; do
    case $yn in
        Yes ) 
            UPDATE_DEV="true"
            break;;
        No ) 
            break;;
    esac
    done
fi

if [ -z "$UPDATE_ATTACK" ] && [ -z ${NO_UPDATE_DEV+x} ]; then   
    echo "Update Attack? [1/2]"
    select yn in "Yes" "No"; do
    case $yn in
        Yes )
            UPDATE_ATTACK="true"
            break;;
        No ) 
            break;;
    esac
    done
fi


pushd Common
    sudo UPDATE_DEV="$UPDATE_DEV" UPDATE_ATTACK="$UPDATE_ATTACK" bash ./root-update.sh
popd

#  _____  ________      __           _               
# |  __ \|  ____\ \    / /          | |              
# | |  | | |__   \ \  / /   ___  ___| |_ _   _ _ __  
# | |  | |  __|   \ \/ /   / __|/ _ \ __| | | | '_ \ 
# | |__| | |____   \  /    \__ \  __/ |_| |_| | |_) |
# |_____/|______|   \/     |___/\___|\__|\__,_| .__/ 
#                                             | |    
#                                             |_| 
if [ ! -z "$UPDATE_DEV" ]; then
    pushd Dev
        bash ./update-tools.sh
    popd
fi
#        _______ _______       _____ _  __           _               
#     /\|__   __|__   __|/\   / ____| |/ /          | |              
#    /  \  | |     | |  /  \ | |    | ' /   ___  ___| |_ _   _ _ __  
#   / /\ \ | |     | | / /\ \| |    |  <   / __|/ _ \ __| | | | '_ \ 
#  / ____ \| |     | |/ ____ \ |____| . \  \__ \  __/ |_| |_| | |_) |
# /_/    \_\_|     |_/_/    \_\_____|_|\_\ |___/\___|\__|\__,_| .__/ 
#                                                             | |    
#                                                             |_|      
if [ ! -z "$UPDATE_ATTACK" ]; then
    pushd Attack
        bash ./update-tools.sh
    popd
fi

echo 'Close current shells and reopen to fix paths.'
echo ' You may need to reboot depending on apt updates.'
