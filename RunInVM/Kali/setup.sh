#! /bin/env bash

#IMPORTANT: DONOT call with `.` use `bash` instead.
#Stop on error
	set -euo pipefail

xfconf-query -c xfce4-notifyd -n -p /do-not-disturb -t bool -s true
#   _____      _                              _               
#  / ____|    | |                            | |              
# | |     ___ | | ___  _   _ _ __    ___  ___| |__   ___  ___ 
# | |    / _ \| |/ _ \| | | | '__|  / _ \/ __| '_ \ / _ \/ __|
# | |___| (_) | | (_) | |_| | |    |  __/ (__| | | | (_) \__ \
#  \_____\___/|_|\___/ \__,_|_|     \___|\___|_| |_|\___/|___/
#                                                             
                                                             
RED=`tput bold && tput setaf 1`
GREEN=`tput bold && tput setaf 2`
YELLOW=`tput bold && tput setaf 3`
BLUE=`tput bold && tput setaf 4`
NC=`tput sgr0`

function RED(){
	echo -e "\n${RED}${1}${NC}"
}
function GREEN(){
	echo -e "\n${GREEN}${1}${NC}"
}
function YELLOW(){
	echo -e "\n${YELLOW}${1}${NC}"
}
function BLUE(){
	echo -e "\n${BLUE}${1}${NC}"
}

#  __  __          _____ _   _                              
# |  \/  |   /\   |_   _| \ | |                             
# | \  / |  /  \    | | |  \| |  _ __ ___   ___ _ __  _   _ 
# | |\/| | / /\ \   | | | . ` | | '_ ` _ \ / _ \ '_ \| | | |
# | |  | |/ ____ \ _| |_| |\  | | | | | | |  __/ | | | |_| |
# |_|  |_/_/    \_\_____|_| \_| |_| |_| |_|\___|_| |_|\__,_|
#                                                           
                                                           
#Make the vars either set or empty string
INSTALL_ATTACK=${INSTALL_ATTACK:-}
INSTALL_DEV=${INSTALL_DEV:-}
NEO4J_PASS=${NEO4J_PASS:-}

BLUE "-------VM Setup--------"
if [ -z "${INSTALL_ATTACK}" ] && [ -z ${NO_INSTALL_ATTACK+x} ]; then   
    BLUE "Install Attack? [1/2]"
    select yn in "Yes" "No"; do
    case $yn in
        Yes )
            INSTALL_ATTACK="true"
            break;;
        No ) 
            break;;
    esac
    done
fi

if [ -z "${INSTALL_DEV}" ] && [ -z ${NO_INSTALL_DEV+x} ]; then  
    BLUE "Install Dev tools(not maintained in a while likely broken)? [1/2]"
    select yn in "Yes" "No"; do
    case $yn in
        Yes ) 
            INSTALL_DEV="true"
            break;;
        No ) 
            break;;
    esac
    done
fi


#        _______ _______       _____ _  __                             
#     /\|__   __|__   __|/\   / ____| |/ /                             
#    /  \  | |     | |  /  \ | |    | ' /   _ __ ___   ___ _ __  _   _ 
#   / /\ \ | |     | | / /\ \| |    |  <   | '_ ` _ \ / _ \ '_ \| | | |
#  / ____ \| |     | |/ ____ \ |____| . \  | | | | | |  __/ | | | |_| |
# /_/    \_\_|     |_/_/    \_\_____|_|\_\ |_| |_| |_|\___|_| |_|\__,_|                                       
#
#

if [ ! -z "${INSTALL_ATTACK}" ]; then
    if [ -z "${NEO4J_PASS}" ];
    then    
        read -sp "Enter new neo4j password: " NEO4J_PASS
    fi

    if [ -z "${NEO4J_PASS}" ];
    then
        RED "neo4j password not set so password will not be changed"
    fi
fi


#   _____ _    _          _____  ______ _____             _               
#  / ____| |  | |   /\   |  __ \|  ____|  __ \           | |              
# | (___ | |__| |  /  \  | |__) | |__  | |  | |  ___  ___| |_ _   _ _ __  
#  \___ \|  __  | / /\ \ |  _  /|  __| | |  | | / __|/ _ \ __| | | | '_ \ 
#  ____) | |  | |/ ____ \| | \ \| |____| |__| | \__ \  __/ |_| |_| | |_) |
# |_____/|_|  |_/_/    \_\_|  \_\______|_____/  |___/\___|\__|\__,_| .__/ 
#                                                                  | |    
#                                                                  |_|  

pushd Common
    sudo INSTALL_DEV="$INSTALL_DEV" INSTALL_ATTACK="$INSTALL_ATTACK" NEO4J_PASS="$NEO4J_PASS" bash ./root-setup.sh
    bash ./box-setup.sh
    bash ./build-tool-folder.sh
popd


#        _______ _______       _____ _  __           _               
#     /\|__   __|__   __|/\   / ____| |/ /          | |              
#    /  \  | |     | |  /  \ | |    | ' /   ___  ___| |_ _   _ _ __  
#   / /\ \ | |     | | / /\ \| |    |  <   / __|/ _ \ __| | | | '_ \ 
#  / ____ \| |     | |/ ____ \ |____| . \  \__ \  __/ |_| |_| | |_) |
# /_/    \_\_|     |_/_/    \_\_____|_|\_\ |___/\___|\__|\__,_| .__/ 
#                                                             | |    
#                                                             |_|  

if [ ! -z "${INSTALL_ATTACK}" ]; then
    BLUE "* Attack setup - Started*"
    pushd Attack
        bash ./build-tool-folder.sh
    popd
    BLUE "* Attack setup - Finished*"
fi


#  _____  ________      __           _               
# |  __ \|  ____\ \    / /          | |              
# | |  | | |__   \ \  / /   ___  ___| |_ _   _ _ __  
# | |  | |  __|   \ \/ /   / __|/ _ \ __| | | | '_ \ 
# | |__| | |____   \  /    \__ \  __/ |_| |_| | |_) |
# |_____/|______|   \/     |___/\___|\__|\__,_| .__/ 
#                                             | |    
#                                             |_| 

if [ ! -z "${INSTALL_DEV}" ]; then
    BLUE "* DEV setup - Started*"
    pushd Dev
        bash ./build-tool-folder.sh
    popd
    BLUE "* Attack setup - Finished*"
fi

if [ -d "priv" ]; then
    pushd priv
        if test -f "./setup-priv.sh"; then
            BLUE "* setup-priv.sh - Started ***"
            bash ./setup-priv.sh
            BLUE "* setup-priv.sh - Finished ***"
        else
            RED "No additional  setup-priv.sh found."
            RED "(this file is not included in the repo, due to holding sensitive infomation)"
        fi
    popd
else
    RED "No additional  priv/setup-priv.sh found."
    RED "(this file is not included in the repo, due to holding sensitive infomation)"
fi

#Copy template bash_aliases if not already configured
if [ ! -f ~/.bash_aliases ] && [ ! -f ~/.bash_aliases ]; then
    cp ./Common/.bash_aliases ~/.bash_aliases
    BLUE "** Copied template .bash_aliases **"
fi
GREEN "Reboot to Complete install"
xfconf-query -c xfce4-notifyd -n -p /do-not-disturb -t bool -s false

# sudo reboot

