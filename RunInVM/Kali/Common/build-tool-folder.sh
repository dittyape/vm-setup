#! /bin/env bash
#IMPORTANT: DONOT call with `.` use `bash` instead.
#Stop on error
	set -euo pipefail

#   _____      _                              _               
#  / ____|    | |                            | |              
# | |     ___ | | ___  _   _ _ __    ___  ___| |__   ___  ___ 
# | |    / _ \| |/ _ \| | | | '__|  / _ \/ __| '_ \ / _ \/ __|
# | |___| (_) | | (_) | |_| | |    |  __/ (__| | | | (_) \__ \
#  \_____\___/|_|\___/ \__,_|_|     \___|\___|_| |_|\___/|___/
#                                                             
                                                             
RED=`tput bold && tput setaf 1`
GREEN=`tput bold && tput setaf 2`
YELLOW=`tput bold && tput setaf 3`
BLUE=`tput bold && tput setaf 4`
NC=`tput sgr0`

function RED(){
	echo -e "\n${RED}${1}${NC}"
}
function GREEN(){
	echo -e "\n${GREEN}${1}${NC}"
}
function YELLOW(){
	echo -e "\n${YELLOW}${1}${NC}"
}
function BLUE(){
	echo -e "\n${BLUE}${1}${NC}"
}

BLUE "** User common setup - Started **"
#Create Tools Dir
mkdir -p ~/Desktop/tools; pushd ~/Desktop/tools

BLUE "*** Debugging tools - Started ***"
	mkdir -p debugging; pushd debugging
		mkdir -p gdb_plugins; pushd gdb_plugins
			# source: https://github.com/apogiatzis/gdb-peda-pwndbg-gef/blob/master/install.sh
			git clone https://github.com/longld/peda.git
			git clone https://github.com/alset0326/peda-arm.git
			git clone https://github.com/pwndbg/pwndbg.git
			git clone https://github.com/hugsy/gef.git
cat <<- "--EOF--" >> ~/.gdbinit
define init-peda
source ~/Desktop/tools/debugging/gdb_plugins/peda/peda.py
end
document init-peda
Initializes the PEDA (Python Exploit Development Assistant for GDB) framework
end

define init-peda-arm
source ~/Desktop/tools/debugging/gdb_plugins/peda-arm/peda-arm.py
end
document init-peda-arm
Initializes the PEDA (Python Exploit Development Assistant for GDB) framework for ARM.
end

define init-peda-intel
source ~/Desktop/tools/debugging/gdb_plugins/peda-arm/peda-intel.py
end
document init-peda-intel
Initializes the PEDA (Python Exploit Development Assistant for GDB) framework for INTEL.
end

define init-pwndbg
source ~/Desktop/tools/debugging/gdb_plugins/pwndbg/gdbinit.py
end
document init-pwndbg
Initializes PwnDBG
end

define init-gef
source ~/Desktop/tools/debugging/gdb_plugins/gef/gef.py
end
document init-gef
Initializes GEF (GDB Enhanced Features)
end
--EOF--
		popd #exit gdb_plugins
	popd #exit debugging
BLUE "*** Debugging tools - Finished ***"

popd #exit ~/Desktop/tools
BLUE "** User Attack setup - Finished **"
