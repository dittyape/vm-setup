#! /bin/env bash

#IMPORTANT: DONOT call with `.` use `bash` instead.
#Stop on error
	set -euo pipefail


#   _____      _                              _               
#  / ____|    | |                            | |              
# | |     ___ | | ___  _   _ _ __    ___  ___| |__   ___  ___ 
# | |    / _ \| |/ _ \| | | | '__|  / _ \/ __| '_ \ / _ \/ __|
# | |___| (_) | | (_) | |_| | |    |  __/ (__| | | | (_) \__ \
#  \_____\___/|_|\___/ \__,_|_|     \___|\___|_| |_|\___/|___/
#                                                             
                                                             
RED=`tput bold && tput setaf 1`
GREEN=`tput bold && tput setaf 2`
YELLOW=`tput bold && tput setaf 3`
BLUE=`tput bold && tput setaf 4`
NC=`tput sgr0`

function RED(){
	echo -e "\n${RED}${1}${NC}"
}
function GREEN(){
	echo -e "\n${GREEN}${1}${NC}"
}
function YELLOW(){
	echo -e "\n${YELLOW}${1}${NC}"
}
function BLUE(){
	echo -e "\n${BLUE}${1}${NC}"
}

#   _____ _    _ _____   ____         _               _    
#  / ____| |  | |  __ \ / __ \       | |             | |   
# | (___ | |  | | |  | | |  | |   ___| |__   ___  ___| | __
#  \___ \| |  | | |  | | |  | |  / __| '_ \ / _ \/ __| |/ /
#  ____) | |__| | |__| | |__| | | (__| | | |  __/ (__|   < 
# |_____/ \____/|_____/ \____/   \___|_| |_|\___|\___|_|\_\
#
#

if [ "$EUID" -ne 0 ]
  then RED "Please run as root (via sudo)"
  exit
fi

bash ./root-box-setup.sh

pushd ..
    #        _______ _______       _____ _  __           _               
    #     /\|__   __|__   __|/\   / ____| |/ /          | |              
    #    /  \  | |     | |  /  \ | |    | ' /   ___  ___| |_ _   _ _ __  
    #   / /\ \ | |     | | / /\ \| |    |  <   / __|/ _ \ __| | | | '_ \ 
    #  / ____ \| |     | |/ ____ \ |____| . \  \__ \  __/ |_| |_| | |_) |
    # /_/    \_\_|     |_/_/    \_\_____|_|\_\ |___/\___|\__|\__,_| .__/ 
    #                                                             | |    
    #                                                             |_|      
    if [ ! -z "${INSTALL_ATTACK}" ]; then
        pushd Attack
            bash ./root-box-setup.sh
        popd
    fi
    #  _____  ________      __           _               
    # |  __ \|  ____\ \    / /          | |              
    # | |  | | |__   \ \  / /   ___  ___| |_ _   _ _ __  
    # | |  | |  __|   \ \/ /   / __|/ _ \ __| | | | '_ \ 
    # | |__| | |____   \  /    \__ \  __/ |_| |_| | |_) |
    # |_____/|______|   \/     |___/\___|\__|\__,_| .__/ 
    #                                             | |    
    #                                             |_| 
    if [ ! -z "${INSTALL_DEV}" ]; then
        pushd Dev
            bash ./root-box-setup.sh
        popd
    fi

    if [ -d "priv" ]; then
        pushd priv
            if test -f "./root-setup-priv.sh"; then
                BLUE "* root-setup-priv.sh - Started ***"
                bash ./root-setup-priv.sh
                BLUE "* root-setup-priv.sh - Finished ***"
            else
                RED "No additional  root-setup-priv.sh found."
                RED "(this file is not included in the repo, due to holding sensitive infomation)"
            fi
        popd
    else
        RED "No additional priv/root-setup-priv.sh found."
        RED "(this file is not included in the repo, due to holding sensitive infomation)"
    fi
popd
