#Attack aliases
alias burp="sh /usr/bin/burpsuite"
alias john-rockyou="john -w /usr/share/wordlists/rockyou.txt"
alias autorecon='f(){ sudo python3 -m autorecon.main $@; sudo chown $SUDO_USER:$SUDO_USER -R "$PWD"; };'
alias aptupdate="sudo apt update -y; sudo apt upgrade -y; sudo apt dist-upgrade -y; sudo apt autoremove -y; sudo searchsploit -u"

#GDB plugins
alias gdb-gef="gdb -q -ex init-gef"
alias gdb-peda="gdb -q -ex init-peda"
alias gdb-peda-arm="gdb -q -ex init-peda-arm"
alias gdb-peda-intel="gdb -q -ex init-peda-intel"
alias gdb-pwndbg="gdb -q -ex init-pwndbg"

#Optout of telematry in .Net Core
    export DOTNET_CLI_TELEMETRY_OPTOUT="true"
#Set GB keyboard map
    setxkbmap -layout gb
# Fix for ecryptfs-mount-private stating no mount
    keyctl link @u @s
