#! /bin/env bash

#IMPORTANT: DONOT call with `.` use `bash` instead.
#Stop on error
	set -euo pipefail
#   _____      _                              _               
#  / ____|    | |                            | |              
# | |     ___ | | ___  _   _ _ __    ___  ___| |__   ___  ___ 
# | |    / _ \| |/ _ \| | | | '__|  / _ \/ __| '_ \ / _ \/ __|
# | |___| (_) | | (_) | |_| | |    |  __/ (__| | | | (_) \__ \
#  \_____\___/|_|\___/ \__,_|_|     \___|\___|_| |_|\___/|___/
#                                                             
                                                             
RED=`tput bold && tput setaf 1`
GREEN=`tput bold && tput setaf 2`
YELLOW=`tput bold && tput setaf 3`
BLUE=`tput bold && tput setaf 4`
NC=`tput sgr0`

function RED(){
	echo -e "\n${RED}${1}${NC}"
}
function GREEN(){
	echo -e "\n${GREEN}${1}${NC}"
}
function YELLOW(){
	echo -e "\n${YELLOW}${1}${NC}"
}
function BLUE(){
	echo -e "\n${BLUE}${1}${NC}"
}


BLUE "** User COMMON setup - Started **"
#   _____ ____  _   _ ______ _____ _____    _____ _                                 
#  / ____/ __ \| \ | |  ____|_   _/ ____|  / ____| |                                
# | |   | |  | |  \| | |__    | || |  __  | |    | |__   __ _ _ __   __ _  ___  ___ 
# | |   | |  | | . ` |  __|   | || | |_ | | |    | '_ \ / _` | '_ \ / _` |/ _ \/ __|
# | |___| |__| | |\  | |     _| || |__| | | |____| | | | (_| | | | | (_| |  __/\__ \
#  \_____\____/|_| \_|_|    |_____\_____|  \_____|_| |_|\__,_|_| |_|\__, |\___||___/
#                                                                    __/ |          
#                                                                   |___/           
BLUE "*** Config Change - Started ***"
#Set keymap to GB
	setxkbmap -layout gb
#Power options- disable auto lock and screensaver.
	xfconf-query -c xfce4-session -p /shutdown/LockScreen -n -t bool -s false
	xfconf-query -c xfce4-power-manager -p /xfce4-power-manager/dpms-enabled -n -t bool -s false
	xfconf-query -c xfce4-power-manager -p /xfce4-power-manager/lock-screen-suspend-hibernate -n -t bool -s false
	xfconf-query -c xfce4-power-manager -p /xfce4-power-manager/logind-handle-lid-switch -n -t bool -s false
	xfconf-query -c xfce4-power-manager -p /xfce4-power-manager/blank-on-ac -n -t int -s 0
	xfconf-query -c xfce4-power-manager -p /xfce4-power-manager/inactivity-on-ac -n -t int -s 0
#show hidden files
	xfconf-query -c thunar -p /last-show-hidden -n -t bool -s true


# Add thumnar shortcuts
if ! grep -Fxq "file:///usr/share/windows-binaries" ~/.config/gtk-3.0/bookmarks; then
	echo "file:///usr/share/windows-binaries" >> ~/.config/gtk-3.0/bookmarks
fi
if ! grep -Fxq "file:///usr/share/nmap/scripts nmap scripts" ~/.config/gtk-3.0/bookmarks; then
	echo "file:///usr/share/nmap/scripts nmap scripts" >> ~/.config/gtk-3.0/bookmarks
fi
if ! grep -Fxq "file:///usr/share/wordlists" ~/.config/gtk-3.0/bookmarks; then
	echo "file:///usr/share/wordlists" >> ~/.config/gtk-3.0/bookmarks
fi

#VScode
	mkdir -p ~/.config/Code/User/
cat <<- "--EOF--" >> ~/.config/Code/User/settings.json
{
    "workbench.enableExperiments": false,
    "json.schemaDownload.enable": false,
    "typescript.disableAutomaticTypeAcquisition": true,
    "settingsSync.keybindingsPerPlatform": false,
    "telemetry.enableTelemetry": false,
    "telemetry.enableCrashReporter": false,
    "npm.fetchOnlinePackageInfo": false,
    "files.autoSave": "afterDelay"
}
--EOF--

BLUE "**** Config ecryptfs - Started ****"
if [ ! -d ~/Private ]; then	
	mkdir -m 700 -p ~/Private
	ln -sf /usr/share/ecryptfs-utils/ecryptfs-mount-private.txt ~/Private/README.txt
	ln -sf /usr/share/ecryptfs-utils/ecryptfs-mount-private.desktop ~/Private/Access-Your-Private-Data.desktop
	chmod 500 ~/Private
fi
BLUE "**** Config ecryptfs - Finished ****"	

cat <<- "--EOF--" >> ~/.zshrc
if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi
--EOF--
BLUE "*** Config Change - Finished ***"
BLUE "** User COMMON setup - Finished **"
