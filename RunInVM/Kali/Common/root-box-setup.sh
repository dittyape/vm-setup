#! /bin/env bash
#IMPORTANT: DONOT call with `.` use `bash` instead.
#Stop on error
	set -euo pipefail

#   _____      _                              _               
#  / ____|    | |                            | |              
# | |     ___ | | ___  _   _ _ __    ___  ___| |__   ___  ___ 
# | |    / _ \| |/ _ \| | | | '__|  / _ \/ __| '_ \ / _ \/ __|
# | |___| (_) | | (_) | |_| | |    |  __/ (__| | | | (_) \__ \
#  \_____\___/|_|\___/ \__,_|_|     \___|\___|_| |_|\___/|___/
#                                                             
                                                             
RED=`tput bold && tput setaf 1`
GREEN=`tput bold && tput setaf 2`
YELLOW=`tput bold && tput setaf 3`
BLUE=`tput bold && tput setaf 4`
NC=`tput sgr0`

function RED(){
	echo -e "\n${RED}${1}${NC}"
}
function GREEN(){
	echo -e "\n${GREEN}${1}${NC}"
}
function YELLOW(){
	echo -e "\n${YELLOW}${1}${NC}"
}
function BLUE(){
	echo -e "\n${BLUE}${1}${NC}"
}

#   _____ _    _ _____   ____         _               _    
#  / ____| |  | |  __ \ / __ \       | |             | |   
# | (___ | |  | | |  | | |  | |   ___| |__   ___  ___| | __
#  \___ \| |  | | |  | | |  | |  / __| '_ \ / _ \/ __| |/ /
#  ____) | |__| | |__| | |__| | | (__| | | |  __/ (__|   < 
# |_____/ \____/|_____/ \____/   \___|_| |_|\___|\___|_|\_\
#
#

if [ "$EUID" -ne 0 ]
  then RED "Please run as root (via sudo)"
  exit
fi


BLUE "** ROOT Common setup - Started **"
BLUE "*** Config Change - Started ***"
#Set keymap to GB
	setxkbmap -layout gb
BLUE "*** Config Change - Finished ***"	
BLUE "*** Enable extra repos - Started ***"
	#Enable kali-bleeding-edge
		echo "deb http://http.kali.org/kali kali-bleeding-edge main contrib non-free" > /etc/apt/sources.list.d/kali-bleeding-edge.list

	#Update Apt
		apt update -y
		apt upgrade -y
		apt dist-upgrade -y
		apt auto-remove -y
BLUE "*** Enable extra repos - Finished ***"


BLUE "*** Install ufw firewall - Started ***"
	apt install -y ufw
	#simple Firewall
		ufw enable
BLUE "*** Install ufw firewall - Finished ***"


BLUE "*** Auto login - Started ***"
	#Setup auto login
		sed -i.bak "s/#autologin-user=/autologin-user=$SUDO_USER/g" /etc/lightdm/lightdm.conf
BLUE "*** auto login - Finished ***"


BLUE "*** Dev APT tools - Started ***"
	#shared dev apt intalls
		apt install -y python3 python3-pip python3-dev python3-venv curl git \
						build-essential libffi-dev zlib1g-dev bison autoconf \
						graphviz flex cmake libeigen3-dev libreadline-dev npm \
						pkg-config curl sed gdb virtualenv
	#notes apt istalls
    	apt install joplin -y #  ecryptfs-utils
		# Temp install for ecryptfs-utils using 111-6 from debian repos.
		pushd /tmp
			wget http://ftp.uk.debian.org/debian/pool/main/e/ecryptfs-utils/libecryptfs1_111-6_amd64.deb
			wget http://ftp.uk.debian.org/debian/pool/main/e/ecryptfs-utils/ecryptfs-utils_111-6_amd64.deb
			apt install ./libecryptfs1_111-6_amd64.deb -y
			apt install ./ecryptfs-utils_111-6_amd64.deb -y
		popd
BLUE "*** Dev APT tools - Finished ***"

BLUE "*** Install Docker - Started ***"	
	apt install -y docker.io
	#Docker
		systemctl start docker
		systemctl enable docker
		groupadd -f docker
		usermod -aG docker $USER
BLUE "*** Install Docker - Finished ***"	

BLUE "*** Install editors - Started ***"
	apt install -y imhex okteta
	BLUE "**** Install vscode - Started ****"	
		#Install Keys and repos.
			apt install -y apt-transport-https dirmngr
			curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > /dev/shm/packages.microsoft.gpg
			install -o root -g root -m 644 /dev/shm/packages.microsoft.gpg /usr/share/keyrings/
			rm /dev/shm/packages.microsoft.gpg
			echo "deb [arch=amd64,arm64,armhf signed-by=/usr/share/keyrings/packages.microsoft.gpg] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list
		#VScode
			apt update -y
			apt install code -y # or code-insiders
	BLUE "**** Install vscode - Finished ****"
BLUE "*** Install editors - Finished ***"

BLUE "*** Install file carving tools - Started ***"
	apt install -y unblob squashfs-tools  # squashfs-tools == unsquashfs
	BLUE "**** Install binwalk - Started ****"
		apt install -y binwalk
		#binwalk additional Deps 
			apt install -y mtd-utils gzip bzip2 tar arj lhasa p7zip p7zip-full cabextract cramfsswap squashfs-tools sleuthkit default-jdk lzop srecord python3-opengl python3-numpy python3-scipy python3-pip zlib1g-dev liblzma-dev liblzo2-dev python3-lzo python3-pycryptodome python3-pyqtgraph python3-capstone python3-cstruct
			mkdir /dev/shm/binwalkdeps; pushd /dev/shm/binwalkdeps
				# Install sasquatch to extract non-standard SquashFS images
					git clone https://github.com/devttys0/sasquatch
					(cd sasquatch && CFLAGS="-fcommon -Wno-misleading-indentation" ./build.sh)
				# Install jefferson to extract JFFS2 file systems
					git clone https://github.com/sviehb/jefferson
					#(cd jefferson && sudo python3 setup.py install)
				# Install ubi_reader to extract UBIFS file systems
					apt install -y python3-poetry 
					git clone https://github.com/jrspruitt/ubi_reader
					(cd ubi_reader && sudo poetry install)
				# Install yaffshiv to extract YAFFS file systems
					git clone https://github.com/devttys0/yaffshiv
					#(cd yaffshiv && sudo python3 setup.py install)
				# Install unstuff (closed source) to extract StuffIt archive files
					#wget -O - <URL_DEAD>/stuffit520.611linux-i386.tar.gz | tar -zxv
					#sudo cp bin/unstuff /usr/local/bin/
			popd; rm -rf /dev/shm/binwalkdeps
	BLUE "**** Install binwalk - Finished ****"	
BLUE "*** Install file carving tools - Finished ***"	

BLUE "*** GDB extensions (PEDA PWNDBG GEF) - Started ***"
	# In case of repo removal
	# Pwndbg: https://github.com/pwndbg/pwndbg
	# Peda: https://github.com/longld/peda
	# GEF: https://github.com/hugsy/gef

	pushd /opt/
		git clone https://github.com/apogiatzis/gdb-peda-pwndbg-gef.git
		pushd gdb-peda-pwndbg-gef
			./install.sh
		popd
	popd
BLUE "*** Config Change - Finished ***"

BLUE "** ROOT Common setup - Finished **"
