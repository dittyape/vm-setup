#! /bin/env bash

#IMPORTANT: DONOT call with `.` use `bash` instead.

#Stop on error
	set -euo pipefail
	
#Create Tools Dir
pushd ~/Desktop/tools
	#file Extension parsers
	pushd fileExtensions
		#.DS_STORE
		pushd DS_STORE
			#DS_Walk
			pushd DS_Walk
				git pull;
			popd
		popd
	popd #exit fileExtensions
	#Access
	pushd Access
		#Access/WebShell
		pushd WebShell
			pushd Web-Shells; 
				git pull; 
			popd
			
			#Access/WebShell/php
			pushd php
				pushd wwwolf-php-webshell; 
					git pull; 
				popd
		
			popd #exit Access/WebShell/php
		popd #exit Access/WebShell
	popd #exit Access
	#CredStuff
	pushd CredStuff
		pushd kerbrute/
			rm *
			curl -s https://api.github.com/repos/ropnop/kerbrute/releases/latest | grep "browser_download_url" | cut -d : -f 2,3 | tr -d \" |  wget -i -
		popd #exit kerbrute
	popd
	
	#Privesc
	pushd Privesc
		#Privesc/Linux
		pushd Linux
			pushd linux-smart-enumeration
				git pull
			popd
			pushd linuxprivchecker
				git pull
			popd
			pushd LinEnum
				git pull
			popd
			pushd kernel-exploits
				git pull
			popd
			pushd linux-exploit-suggester
				git pull
			popd
			pushd SUDO_KILLER
				git pull
			popd
		
			pushd pspy
				rm *
				curl -s https://api.github.com/repos/DominicBreuker/pspy/releases/latest | grep "browser_download_url" | cut -d : -f 2,3 | tr -d \" |  wget -i -
			popd #exit Privesc/Linux/pspy
		popd #exit Privesc/Linux
		#Privesc/Windows
		pushd Windows
	
			pushd Powerless
				git pull
			popd
			pushd windows-kernel-exploits
				git pull
			popd
			pushd wesng
				git pull
			popd
			pushd BloodHound
				git pull
			popd
		popd #exit Privesc/Windows
	popd #exit Privesc
	#SQLi
	pushd SQLi
		#SQLi/NoSql
		pushd NoSql
			pushd Nosql-MongoDB-injection-username-password-enumeration
				git pull
			popd
		popd #exit SQLi/NoSql
	popd #exit SQLi
	
	
	#RE
	pushd RE
		#Native
		#pushd Native
		#	pushd retdec # Stopped working
		#		git pull
		#		pushd build
		#			cmake .. -DCMAKE_INSTALL_PREFIX="$(pwd)"
		#			make -j4
		#			make install
		#		popd
		#	popd	
		#popd #exit RE/Native
	
		#.Net
		pushd dotNet
			pushd AvaloniaILSpy
				bash ./build.sh
			popd
		popd #exit RE/dotnet
		
		
		pushd java
			pushd bytecodeviewer
				rm *
				curl -s https://api.github.com/repos/Konloch/bytecode-viewer/releases/latest | grep "browser_download_url" | cut -d : -f 2,3 | tr -d \" |  wget -i -
		
			popd #exit RE/java/bytecodeviewer
		popd #exit RE/java
	
	popd #exit RE
	
	
	pushd ysoserial
		pushd dotnet
			rm *
			curl -s https://api.github.com/repos/pwntester/ysoserial.net//releases/latest | grep "browser_download_url" | cut -d : -f 2,3 | tr -d \" |  wget -i -
		popd
		pushd java
			rm *.jar
			wget https://jitpack.io/com/github/frohoff/ysoserial/master-SNAPSHOT/ysoserial-master-SNAPSHOT.jar
		popd
	popd
popd
	
echo 'Mannually Update:'
echo '  idapro'	

