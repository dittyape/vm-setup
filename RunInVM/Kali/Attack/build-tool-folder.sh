#! /bin/env bash
#IMPORTANT: DONOT call with `.` use `bash` instead.
#Stop on error
	set -euo pipefail

#   _____      _                              _               
#  / ____|    | |                            | |              
# | |     ___ | | ___  _   _ _ __    ___  ___| |__   ___  ___ 
# | |    / _ \| |/ _ \| | | | '__|  / _ \/ __| '_ \ / _ \/ __|
# | |___| (_) | | (_) | |_| | |    |  __/ (__| | | | (_) \__ \
#  \_____\___/|_|\___/ \__,_|_|     \___|\___|_| |_|\___/|___/
#                                                             
                                                             
RED=`tput bold && tput setaf 1`
GREEN=`tput bold && tput setaf 2`
YELLOW=`tput bold && tput setaf 3`
BLUE=`tput bold && tput setaf 4`
NC=`tput sgr0`

function RED(){
	echo -e "\n${RED}${1}${NC}"
}
function GREEN(){
	echo -e "\n${GREEN}${1}${NC}"
}
function YELLOW(){
	echo -e "\n${YELLOW}${1}${NC}"
}
function BLUE(){
	echo -e "\n${BLUE}${1}${NC}"
}

BLUE "** User Attack setup - Started **"
#Create Tools Dir
mkdir -p ~/Desktop/tools;

BLUE "*** copy Burp Plugin tool - Started ***"
cp -r burp_plugins ~/Desktop/tools;
BLUE "*** copy Burp Plugin tool - Finished ***"

pushd ~/Desktop/tools
BLUE "*** Burp Plugin Download - Started ***"
	pushd ~/Desktop/tools/burp_plugins
		bash ./run.sh
	popd
BLUE "*** Burp Plugin Download - Finished ***"

BLUE "*** Windows vm tools - Started ***"
	#Skip errors on win tools as just helper really
	set +eu	
		#Windows VM tools
		mkdir windowsVmTools; pushd windowsVmTools
			mkdir immunity; pushd immunity
				git clone https://github.com/corelan/mona.git
			popd #exit immunity
			mkdir vcruntime; pushd vcruntime
				wget https://aka.ms/vs/16/release/vc_redist.x64.exe
				wget https://aka.ms/vs/16/release/vc_redist.x86.exe
			popd #exit vcruntime
			mkdir java; pushd java
				wget https://aka.ms/download-jdk/microsoft-jdk-11.0.12.7.1-windows-x64.msi
				wget https://aka.ms/download-jdk/microsoft-jdk-16.0.2.7.1-windows-x64.msi
			popd #
			mkdir windows10SDK; pushd windows10SDK
				wget https://go.microsoft.com/fwlink/?linkid=2164360
			popd #exit windows10SDK
		popd #exit windowsVmTools
	set -eu
BLUE "*** Windows vm tools - Finished ***"

BLUE "*** File Extension tools - Started ***"
	#file Extension parsers
	mkdir fileExtensions; pushd fileExtensions
		#.DS_STORE
		mkdir DS_STORE; pushd DS_STORE
			git clone https://github.com/Keramas/DS_Walk
		popd
	popd #exit fileExtensions
BLUE "*** File Extension tools - Finished ***"

BLUE "*** Webshell tools - Started ***"
	#WebShell
	mkdir WebShell; pushd WebShell
	git clone https://github.com/Xh4H/Web-Shells.git

		#WebShell/php
		mkdir php; pushd php
		git clone https://github.com/WhiteWinterWolf/wwwolf-php-webshell.git

		popd #exit WebShell/php
	popd #exit WebShell
BLUE "*** Webshell tools - Finished ***"

BLUE "*** Cred Stuffing tools - Started ***"
	#CredStuff
	mkdir CredStuff; pushd CredStuff
		#Kerbrute
		mkdir kerbrute; pushd kerbrute/
		curl -s https://api.github.com/repos/ropnop/kerbrute/releases/latest | grep "browser_download_url" | cut -d : -f 2,3 | tr -d \" |  wget -i -   #" (close qoate to help syntax highlighting)
		popd #exit kerbrute
	popd #exit credstuff
BLUE "*** Cred Stuffing tools - Finished ***"

BLUE "*** Privage escalation tools - Started ***"
	#Privesc
	mkdir Privesc; pushd Privesc
		#Privesc/Linux
		mkdir Linux; pushd Linux

		git clone https://github.com/diego-treitos/linux-smart-enumeration.git
		git clone https://github.com/linted/linuxprivchecker.git
		git clone https://github.com/rebootuser/LinEnum.git
		git clone https://github.com/lucyoa/kernel-exploits.git
		git clone https://github.com/mzet-/linux-exploit-suggester.git
		git clone https://github.com/TH3xACE/SUDO_KILLER.git

		#Privesc/Linux/pspy
		mkdir pspy; pushd pspy
			curl -s https://api.github.com/repos/DominicBreuker/pspy/releases/latest | grep "browser_download_url" | cut -d : -f 2,3 | tr -d \" |  wget -i -   #" (close qoate to help syntax highlighting)
			popd #exit Privesc/Linux/pspy
		popd #exit Privesc/Linux
		
		#Privesc/Windows
		mkdir Windows; pushd Windows

		git clone https://github.com/M4ximuss/Powerless.git
		git clone https://github.com/SecWiki/windows-kernel-exploits.git
		git clone https://github.com/bitsadmin/wesng.git
		git clone https://github.com/BloodHoundAD/BloodHound.git

		popd #exit Privesc/Windows
	popd #exit Privesc

BLUE "*** Privage escalation tools - Finished ***"

BLUE "*** Sqli tools - Started ***"
	#SQLi
	mkdir SQLi; pushd SQLi
		#SQLi/NoSql
		mkdir NoSql; pushd NoSql
		git clone https://github.com/an0nlk/Nosql-MongoDB-injection-username-password-enumeration.git
		popd #exit SQLi/NoSql
	popd #exit SQLi

BLUE "*** Sqli tools - Finished ***"

BLUE "*** RE tools - Started ***"
	#RE
	mkdir RE; pushd RE
		#Native
		#mkdir Native; pushd Native
		#retdec - stopped working and "The RetDec project is currently in a limited maintenance mode due to a lack of resources"
		#	git clone https://github.com/avast/retdec
		#	pushd retdec
		#	mkdir build && pushd build
		#	cmake .. -DCMAKE_INSTALL_PREFIX="$(pwd)"
		#	make -j4
		#	make install
		#
		#	popd # Exit build
		#	popd # exit retdec
		#popd #exit RE/Native

		#.net
		mkdir dotNet; pushd dotNet
			#AvaloniILSpy
			curl -s https://api.github.com/repos/icsharpcode/AvaloniaILSpy/releases/latest | grep "browser_download_url" | cut -d : -f 2,3 | tr -d \" |  wget -i -   #" (close qoate to help syntax highlighting)
		popd #exit RE/dotnet

		#Java
		mkdir java; pushd java
			#bytecodeviewer
			mkdir bytecodeviewer; pushd bytecodeviewer
				curl -s https://api.github.com/repos/Konloch/bytecode-viewer/releases/latest | grep "browser_download_url" | cut -d : -f 2,3 | tr -d \" |  wget -i -
			popd #exit RE/java/bytecodeviewer
		popd #exit RE/java
	popd #exit RE
BLUE "*** RE tools - Finished ***"

BLUE "*** ysoserial - Started ***"
	#Ysoserial
	mkdir ysoserial; pushd ysoserial
		#.net
		mkdir dotnet; pushd dotnet
			curl -s https://api.github.com/repos/pwntester/ysoserial.net/releases/latest | grep "browser_download_url" | cut -d : -f 2,3 | tr -d \" |  wget -i -
		popd
		#java
		mkdir java; pushd java
			wget https://github.com/frohoff/ysoserial/releases/latest/download/ysoserial-all.jar
		popd #exit ysoserial/java
	popd #exit ysoserial
BLUE "*** ysoserial - Finished ***"

popd #exit ~/Desktop/tools
BLUE "** User Attack setup - Finished **"
