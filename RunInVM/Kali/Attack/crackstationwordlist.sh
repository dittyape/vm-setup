#! /bin/env bash
#IMPORTANT: DONOT call with `.` use `bash` instead.
#Stop on error
	set -euo pipefail
#crackstation wordlist
	wget https://crackstation.net/files/crackstation.txt.gz -O ~/Desktop/crackstation.txt.gz
	gzip -d ~/Desktop/crackstation.txt.gz -c > ~/Desktop/crackstation.txt
	sudo mv ~/Desktop/crackstation.txt /usr/share/wordlists
	rm ~/Desktop/crackstation.txt.gz
