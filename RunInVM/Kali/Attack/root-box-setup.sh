#! /bin/env bash
#IMPORTANT: DONOT call with `.` use `bash` instead.
#Stop on error
	set -euo pipefail

#   _____      _                              _               
#  / ____|    | |                            | |              
# | |     ___ | | ___  _   _ _ __    ___  ___| |__   ___  ___ 
# | |    / _ \| |/ _ \| | | | '__|  / _ \/ __| '_ \ / _ \/ __|
# | |___| (_) | | (_) | |_| | |    |  __/ (__| | | | (_) \__ \
#  \_____\___/|_|\___/ \__,_|_|     \___|\___|_| |_|\___/|___/
#                                                             
                                                             
RED=`tput bold && tput setaf 1`
GREEN=`tput bold && tput setaf 2`
YELLOW=`tput bold && tput setaf 3`
BLUE=`tput bold && tput setaf 4`
NC=`tput sgr0`

function RED(){
	echo -e "\n${RED}${1}${NC}"
}
function GREEN(){
	echo -e "\n${GREEN}${1}${NC}"
}
function YELLOW(){
	echo -e "\n${YELLOW}${1}${NC}"
}
function BLUE(){
	echo -e "\n${BLUE}${1}${NC}"
}

#   _____ _    _ _____   ____         _               _    
#  / ____| |  | |  __ \ / __ \       | |             | |   
# | (___ | |  | | |  | | |  | |   ___| |__   ___  ___| | __
#  \___ \| |  | | |  | | |  | |  / __| '_ \ / _ \/ __| |/ /
#  ____) | |__| | |__| | |__| | | (__| | | |  __/ (__|   < 
# |_____/ \____/|_____/ \____/   \___|_| |_|\___|\___|_|\_\
#
#

if [ "$EUID" -ne 0 ]
  then RED "Please run as root (via sudo)"
  exit
fi


BLUE "** ROOT Attack setup - Started **"

BLUE "*** Set ufw rules - Started ***"
	ufw allow 1337
	ufw allow 1338
BLUE "*** Set ufw rules - Finished ***"


BLUE "*** dev(attack) apt installs - Started ***"
#dev(attack) apt installs
	apt install -y libssl-dev yasm libgmp-dev libpcap-dev libbz2-dev \
				 ocl-icd-opencl-dev opencl-headers pocl-opencl-icd \
				 openssl automake libtool m4 doxygen golang  libimage-exiftool-perl
BLUE "*** dev(attack) apt installs - Finished ***"

BLUE "*** pentest APT tools - Started ***"
	# pentest tools
		apt install -y offsec-pwk radare2 \
			rizin-cutter unix-privesc-check powersploit \
			bytecode-viewer bloodhound upx-ucl \
			impacket-scripts evil-winrm john peass
		arch=$(uname -i)
		if [[ $arch == x86_64* ]] || [[ $arch == i*86 ]]; then
			apt install -y edb-debugger
		fi
	# ghidra                     
		apt install -y ghidra
BLUE "*** pentest APT tools - Finished ***"

BLUE "*** autorecon install - Started ***"
	# auto recon deps
		apt install -y feroxbuster gobuster
	# auto recon recommended
		apt install -y wpscan ldap-utils sqsh hydra medusa rsync nfs-common 
	apt install -y autorecon
BLUE "*** autorecon install - Finished ***"


BLUE "*** wordlist install - Started ***"
	#Wordlist update and extract rockyou
		#wordlist deps
			apt install -y dirb dirbuster fern-wifi-cracker metasploit-framework  wfuzz seclists sqlmap dnsmap  nmap set termineter  wpscan
		apt install wordlists --reinstall -y
		gzip -d /usr/share/wordlists/rockyou.txt.gz -c > /usr/share/wordlists/rockyou.txt
BLUE "*** wordlist install - Finished ***"


BLUE "*** searchsploit update - Started ***"
	set +eu	
	#update seachsploit
		searchsploit -u
	set -eu
BLUE "*** searchsploit update - Finished ***"


BLUE "*** pwntools install - Started ***"
	#update pip and install pip tools
	apt install -y python3-pwntools
BLUE "*** pwntools install - Finished ***"

BLUE "*** .NET and mono - Started ***"
	# .Net core - bugged on arm
	arch=$(uname -i)
	if [[ $arch == x86_64* ]] || [[ $arch == i*86 ]]; then
		wget https://packages.microsoft.com/config/debian/11/packages-microsoft-prod.deb -O /dev/shm/packages-microsoft-prod.deb
		dpkg -i /dev/shm/packages-microsoft-prod.deb
		rm /dev/shm/packages-microsoft-prod.deb
		apt-get update;
		apt-get install -y dotnet-sdk-6.0 aspnetcore-runtime-6.0
	fi


	#mono - Disabled due to dependancy error
	#	apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF
	#	echo "deb https://download.mono-project.com/repo/debian stable-buster main" > /etc/apt/sources.list.d/mono-official-stable.list

	#	apt update
	#	apt install -y monodevelop

	#Cleanup
		apt autoremove -y
BLUE "*** .NET and mono - Finished ***"

if [ ! -z "${NEO4J_PASS}" ]; then
	BLUE "*** Set neo4j pass - Started ***"
		/usr/share/neo4j/bin/neo4j-admin set-initial-password $NEO4J_PASS
	BLUE "*** Set neo4j pass - Finished ***"
fi

BLUE "** ROOT Attack setup - Finished **"
