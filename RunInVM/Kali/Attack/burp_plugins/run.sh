#! /bin/env bash
#IMPORTANT: DONOT call with `.` use `bash` instead.
#Stop on error
	set -euo pipefail

    virtualenv venv
    . venv/bin/activate
    pip install -r requirements.txt
    python bap-downloader.py
    deactivate
    