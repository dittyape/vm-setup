import requests
import shutil
from bs4 import BeautifulSoup
from pathlib import Path

BASE_URL = 'https://portswigger.net/bappstore/'

BAPP_PATH=Path('bapps')
# Add name to restrict downloading that type
BLOCKED_VERSIONS=["enterprise", "professional"]

# Orginal list (with added community/pro/enterprise flags)
BAPP_IDS = [
    ('Active Scan++', 'active_scan_plus_plus.bapp', '3123d5b5f25c4128894d97ea1acc4976', False, True,False),
    ('Add Custom Header', 'add_custom_header.bapp', '807907f5380c4cb38748ef4fc1d8cdbc', True, True,False),
    ('AuthMatrix', 'auth_matrix.bapp', '30d8ee9f40c041b0bfec67441aad158e', True, True,False),
    ('Autorize', 'autorize.bapp', 'f9bbac8c4acf4aefa4d7dc92a991af2f', True, True,False),
    ('Blackslash Powered Scanner', 'backslash_powered_scanner.bapp', '9cff8c55432a45808432e26dbb2b41d8', False, True,True),
    ('Collaborator Everywhere', 'collaborator_everywhere.bapp', '2495f6fb364d48c3b6c984e226c02968', False, True, False),
    ('Content Type Converter', 'content_type_converter.bapp', 'db57ecbe2cb7446292a94aa6181c9278', True, True, False),
    ('CORS*, Additional CORS Checks', 'cors.bapp', '420a28400bad4c9d85052f8d66d3bbd8', False, True, False),
    ('GraphQL Raider', 'graphql_raider.bapp', '4841f0d78a554ca381c65b26d48207e6', False, True, False),
    ('Hackvertor', 'hackvertor.bapp', '65033cbd2c344fbabe57ac060b5dd100', True, True, False),
    ('HTTP Request Smuggler', 'http_request_smuggler.bapp', 'aaaa60ef945341e8a450217a54a11646', True, True, False),
    ('Java Deserialization Scanner', 'java_deserialization_scanner.bapp', '228336544ebe4e68824b5146dbbd93ae', True, True, False),
    ('JSON Web Tokens', 'json_web_tokens.bapp', 'f923cbf91698420890354c1d8958fee6', True, True, False),
    ('Log4Shell Everywhere', 'log4shell_everywhere.bapp', '186be35f6e0d418eb1f6ecf1cc66a74d', False, True, False),
    ('OpenAPI Parser', 'open_api_parser.bapp', '6bf7574b632847faaaa4eb5e42f1757c', True, True, False),
    ('Param Miner', 'param_miner.bapp', '17d2949a985c4b7ca092728dba871943', True, True, False),
    ('Proxy Auto Config', 'proxy_auto_config.bapp', '7b3eae07aa724196ab85a8b64cd095d1', True, True, False),
    ('Reflected Parameters', 'reflected_parameters.bapp', '8e8f6bb313db46ba9e0a7539d3726651', False, True, False),
    ('Request Minimizer', 'request_minimizer.bapp', 'cc16f37549ff416b990d4312490f5fd1', True, True, False),
    ('Request Randomizer', 'request_randomizer.bapp', '36d6d7e35dac489b976c2f120ce34ae2', True, True, False),
    ('Retire.js', 'retire_js.bapp', '36238b534a78494db9bf2d03f112265c', False, True, False),
    ('SAML Raider', 'saml_raider.bapp', 'c61cfa893bb14db4b01775554f7b802e', True, True, False),
    ('Taborator', 'taborator.bapp', 'c9c37e424a744aa08866652f63ee9e0f', False, True, False),
    ('Turbo Intruder', 'turbo_intruder.bapp', '9abaa233088242e8be252cd4ff534988', True, True, False),
    ('Upload Scanner', 'upload_scanner.bapp', 'b2244cbb6953442cb3c82fa0a0d908fa', False, True, False),
]
# My favoraits
BAPP_IDS+=[
    #('Java Deserialization Scanner', 'java_deserialization_scanner.bapp', '228336544ebe4e68824b5146dbbd93ae', True, True, False),
    ('SQLiPy Sqlmap Integration', 'sqlipy_sqlmap_integration.bapp', 'f154175126a04bfe8edc6056f340f52e', True, True, False),
]

def extract_bapp_url(bapp_id:str) -> str:

    res = requests.get(BASE_URL + bapp_id)
    soup = BeautifulSoup(res.content, features='html.parser')

    return soup.find_all('a', id='DownloadedLink')[0]['href']


def download_bapp(bapp_name, bapp_url, versions):
    if len(versions)>0:
        with requests.get(bapp_url, stream=True) as r:
            for version in versions:
                version_dir = BAPP_PATH.joinpath(version)
                version_dir.mkdir(parents=True, exist_ok=True)
                with open(version_dir.joinpath(bapp_name), 'wb') as f:
                    shutil.copyfileobj(r.raw, f)


if __name__ == '__main__':
    # Download extensions
    for bapp_name, bapp_file_name, bapp_id, community, professional, enterprise in BAPP_IDS:
        try:
            print('[*] Downloading {}...'.format(bapp_name))
            bapp_href = extract_bapp_url(bapp_id)
            versions = []
            if community:
                versions.append("community")
            if professional:
                versions.append("professional")
            if enterprise:
                versions.append("enterprise")

            if BLOCKED_VERSIONS and len(BLOCKED_VERSIONS)>0:
                versions = [ v for v in versions if v not in BLOCKED_VERSIONS]
                
            download_bapp(bapp_file_name, bapp_href, versions)
        except Exception as e:
            print('[!] Failed to download {}.'.format(bapp_name))
            raise e