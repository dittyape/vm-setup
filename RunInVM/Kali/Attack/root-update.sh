#! /bin/env bash
#IMPORTANT: DONOT call with `.` use `bash` instead.
#Stop on error
	set -euo pipefail

#   _____      _                              _               
#  / ____|    | |                            | |              
# | |     ___ | | ___  _   _ _ __    ___  ___| |__   ___  ___ 
# | |    / _ \| |/ _ \| | | | '__|  / _ \/ __| '_ \ / _ \/ __|
# | |___| (_) | | (_) | |_| | |    |  __/ (__| | | | (_) \__ \
#  \_____\___/|_|\___/ \__,_|_|     \___|\___|_| |_|\___/|___/
#                                                             
                                                             
RED=`tput bold && tput setaf 1`
GREEN=`tput bold && tput setaf 2`
YELLOW=`tput bold && tput setaf 3`
BLUE=`tput bold && tput setaf 4`
NC=`tput sgr0`

function RED(){
	echo -e "\n${RED}${1}${NC}"
}
function GREEN(){
	echo -e "\n${GREEN}${1}${NC}"
}
function YELLOW(){
	echo -e "\n${YELLOW}${1}${NC}"
}
function BLUE(){
	echo -e "\n${BLUE}${1}${NC}"
}

#   _____ _    _ _____   ____         _               _    
#  / ____| |  | |  __ \ / __ \       | |             | |   
# | (___ | |  | | |  | | |  | |   ___| |__   ___  ___| | __
#  \___ \| |  | | |  | | |  | |  / __| '_ \ / _ \/ __| |/ /
#  ____) | |__| | |__| | |__| | | (__| | | |  __/ (__|   < 
# |_____/ \____/|_____/ \____/   \___|_| |_|\___|\___|_|\_\
#
#

if [ "$EUID" -ne 0 ]
  then RED "Please run as root (via sudo)"
  exit
fi


BLUE "** ROOT update - Started **"
#
# TODO: add progress logging
#


#Update apt
	apt install wordlists --reinstall -y

#Update pip
	python3 -m pip install --upgrade git+https://github.com/Tib3rius/AutoRecon.git
	python3 -m pip install --upgrade pwntools

#update seachsploit
	searchsploit -u


BLUE "** ROOT update - Finished **"
