
## Extending with private steps
If you (like i) have addtional scripts which you wish to run but do not wish to store in git there I have added functionallity.

I tend to use this for setting up my automated backups of the ecrypt-fs folder stored at ```~/.Private``` to a VCS.

For this store additional resources in ```/priv/``` these will be ignored by the .gitignore.
If you create an entry point of ```priv/setup_priv.sh``` this will be called after the setup has completed.
This allows for further customization of the setup in an automated way.

I prefer this such that as and when a new version of kali arrives i can trash my old vm and in just a few moments have a new vm setup ready to go. 

## Install Manually
Running the following will display a menu wich will ask if you wish to install the attack tools or dev tools and any additonal configs 
```bash
bash ./setup.sh
```

## Automated install
It is possible to pre-answer the questions in the menu by setting variables. The variables and their meanings are as follows.

| --- | --- | 
| Arg name | Notes |
| --- | --- |
| INSTALL_DEV | Indicates to install the dev tools | 
| NO_INSTALL_ATTACK | Skips the install the dev tools question, hence indicateing not to install dev tools |
| INSTALL_ATTACK | Indicates to install the attack tools | 
| NO_INSTALL_ATTACK | Skips the install the dev tools question, hence indicateing not to install dev tools |
| NEO4J_PASS | Only used if installing Attack, this is the password that teh neo4j user in the neo4j database will be set( used for bloodhound) |



## Update Manually
Running the following will display a menu wich will ask if you wish to update the attack tools or dev tools and any additonal configs 
```bash
bash ./setup.sh
```


## Automated Update
It is possible to pre-answer the questions in the menu by setting variables. The variables and their meanings are as follows.

| --- | --- | 
| Arg name | Notes |
| --- | --- |
| UPDATE_DEV | Indicates to update the dev tools | 
| NO_UPDATE_ATTACK | Skips the update the dev tools question, hence indicateing not to update dev tools |
| UPDATE_ATTACK | Indicates to update the attack tools | 
| NO_UPDATE_ATTACK | Skips the update the dev tools question, hence indicateing not to update dev tools |
```