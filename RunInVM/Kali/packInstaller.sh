
#! /bin/env bash
#IMPORTANT: DONOT call with `.` use `bash` instead.
#Stop on error
	set -euo pipefail

tar -zcf Release.tar.gz ./*

cat <<- "--EOF--" > Release.packed.sh
#!/bin/bash
#Stop on error
set -euo pipefail
echo "Packed Installer"
export TMPDIR=`mktemp -d /tmp/selfextract.XXXXXX`
ARCHIVE=`awk '/^__ARCHIVE_BELOW__/ {print NR + 1; exit 0; }' $0`
tail -n+$ARCHIVE $0 | base64 -d - | tar xz -C $TMPDIR
pushd $TMPDIR
bash ./setup.sh
popd
rm -rf $TMPDIR
exit 0
__ARCHIVE_BELOW__
--EOF--

base64 Release.tar.gz >> Release.packed.sh
rm Release.tar.gz
