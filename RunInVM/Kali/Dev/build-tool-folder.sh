#! /bin/env bash

#IMPORTANT: DONOT call with `.` use `bash` instead.
#Stop on error
	set -euo pipefail
#Create Tools Dir
pushd ~/Desktop/
mkdir -p tools; pushd tools
	#hardware
	mkdir hardware; pushd hardware
		#fpgas
		mkdir fpga; pushd fpga
			git clone https://github.com/open-tool-forge/summon-fpga-tools.git summon-fpga-tools
			pushd summon-fpga-tools
				./summon-fpga-tools.sh PREFIX=${HOME}/Desktop/tools/hardware/fpga/sft
			popd
			git clone https://github.com/FPGAwars/icestudio.git
			pushd icestudio
				npm install
			popd
			mkdir icestudio-collections; pushd icestudio-collections
				git clone https://github.com/FPGAwars/iceInputs.git
				git clone https://github.com/FPGAwars/iceFF.git
				git clone https://github.com/FPGAwars/iceGates.git
				git clone https://github.com/FPGAwars/iceGates-TB.git
				git clone https://github.com/FPGAwars/iceFF-TB.git
				git clone https://github.com/FPGAwars/Collection-Jedi.git
				git clone https://github.com/FPGAwars/iceSignals.git
				git clone https://github.com/FPGAwars/iceWires.git
				git clone https://github.com/FPGAwars/iceK.git
				git clone https://github.com/FPGAwars/Collection-stdio.git
				git clone https://github.com/FPGAwars/icestudio-blocks.git
				git clone https://github.com/FPGAwars/iceMem.git
				git clone https://github.com/FPGAwars/iceMeasure.git
				git clone https://github.com/FPGAwars/iceSynth-collection.git
				git clone https://github.com/FPGAwars/Collection-Jedi-Test.git
				git clone https://github.com/FPGAwars/FPGA-peripherals.git
				git clone https://github.com/FPGAwars/icebreaker-collection.git
				git clone https://github.com/FPGAwars/collection-generic.git
				git clone https://github.com/FPGAwars/collection-logic.git				
			popd
		popd
	popd
popd
