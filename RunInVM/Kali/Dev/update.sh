#! /bin/env bash

#IMPORTANT: DONOT call with `.` use `bash` instead.
#Stop on error
	set -euo pipefail

#Create Tools Dir
pushd ~/Desktop/tools
	#hardware
	pushd hardware
		#fpgas
		pushd fpga
			pushd summon-fpga-tools
				git pull
				./summon-fpga-tools.sh PREFIX=${HOME}/Desktop/tools/hardware/fpga/sft
			popd
			pushd icestudio
				git pull
				npm install
			popd
		popd
	popd
popd
