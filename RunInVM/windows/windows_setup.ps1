# Install chocolatey
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))

# Install chocolatey packages
choco install 7zip.install --version 22.1 -y
choco install python --version 3.11.0 -y
choco install git --version 2.39.1 -y
choco install sysinternals --version 2022.11.28 -y
choco install javaruntime --version 8.0.231 -y
choco install vscode --version 1.74.3 -y
choco install putty.install --version 0.78 -y
choco install jdk8 --version 8.0.211 -y
choco install filezilla --version 3.62.2 -y
choco install winscp.install --version 5.21.6 -y
choco install postman --version 10.7.0 -y
choco install everything --version 1.4.11022 -y
choco install graphviz --version 7.1.0 -y
choco install jdk11 --version 11.0.2.9 -y
choco install fiddler --version 5.0.20211.51073 -y
choco install mysql.workbench --version 8.0.31 -y
choco install vcredist-all --version 1.0.1 -y
choco install windbg --version 10.0.10586.15 -y
choco install mobaxterm --version 22.3.0 -y
choco install ilspy --version 7.2.1 -y
choco install processhacker.install --version 2.39 -y
choco install lockhunter --version 3.4.3.20220526 -y
choco install hxd --version 2.5.0.0 -y
choco install meld --version 3.22.0 -y
choco install burp-suite-free-edition --version 2022.12.4 -y
choco install sqlitebrowser --version 3.12.2 -y
choco install joplin --version 2.8.8 -y
choco install zap --version 2.12.0.20221127 -y
choco install virtualclonedrive --version 5.5.2.0 -y
choco install x64dbg.portable --version 20230115.1529 -y
choco install vscode-python --version 2022.19.13071014 -y
choco install reshack.portable --version 5.1.8.360 -y
choco install ghidra --version 10.2.2 -y
