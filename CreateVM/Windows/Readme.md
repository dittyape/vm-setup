# Create kali vm

# Windows host using VMware
First Install VMware (last tested with vmware 17) - https://www.vmware.com/uk/products/workstation-pro.html
Then run:
```
powershell -exec bypass -c "iwr('https://gitlab.com/dittyape/vm-setup/-/raw/main/CreateVM/kali_on_windows.ps1?ref_type=heads')|iex"
```
or a local copy:
```
powershell -exec bypass -File .\kali_on_windows.ps1
```

# Windows host using Hyper-V
First Install Hyper-V with:
```
Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V-All
```

Then run:
```
powershell -exec bypass -c "iwr('https://gitlab.com/dittyape/vm-setup/-/raw/main/CreateVM/Windows/kali_on_windows.ps1?ref_type=heads')|iex"
```
or a local copy with answers piped in to script:
```
@'
vmware

Y
https://cdimage.kali.org/current/
kaliuser
toor

'@ | powershell -exec bypass -File .\kali_on_windows.ps1
```

As the Hyper-v functionallity has not been finished once ran 
please create a VM using the iso found at `~/Downloads/kali/kali-modified.iso`
(or if you selected a different working dir in that working dir)

# Windows host using VirtualBox
First Install VirtualBox

Then run:
```
powershell -exec bypass -c "iwr('https://gitlab.com/dittyape/vm-setup/-/raw/main/CreateVM/Windows/kali_on_windows.ps1?ref_type=heads')|iex"
```
or a local copy:
```
powershell -exec bypass -File .\kali_on_windows.ps1
```

As the VirtualBox functionallity has not been finished once ran 
please create a VM using the iso found at `~/Downloads/kali/kali-modified.iso`
(or if you selected a different working dir in that working dir)

# Linux Host
Currently no Auto Script exists for linux 
As such create a kali vm using the netinstall ISO.
When asked what to install deselect all but the desktop (XFCE).
Once the minimal kali is installed then clone this repo and run the `bash ./setup.sh` from the repos dir

# Mac host
There are no plans to create an auto script for mac.
Also this repo is not supported by the Arm Macs.
As such with an x86_64 mac create a kali vm using the netinstall ISO
When asked what to install deselect all but the desktop (XFCE).
Once the minimal kali is installed then clone this repo and run the `bash ./setup.sh` from the repos dir
