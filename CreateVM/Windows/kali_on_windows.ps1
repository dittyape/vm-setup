function Read-Choice(
   [Parameter(Mandatory)][string]$Message,
   [Parameter(Mandatory)][string[]]$Choices,
   [Parameter(Mandatory)][string]$DefaultChoice,
   [Parameter()][string]$Question='Are you sure you want to proceed?'
) 
{
    $defaultIndex = $Choices.IndexOf($DefaultChoice)
    if ($defaultIndex -lt 0) {
        throw "$DefaultChoice not found in choices"
    }

    $choiceObj = New-Object Collections.ObjectModel.Collection[Management.Automation.Host.ChoiceDescription]

    foreach($c in $Choices) {
        $choiceObj.Add((New-Object Management.Automation.Host.ChoiceDescription -ArgumentList $c))
    }

    $decision = $Host.UI.PromptForChoice($Message, $Question, $choiceObj, $defaultIndex)
    return $Choices[$decision]
}

function Open-kali-iso-dialog
{
    try{
        $FileBrowser = New-Object System.Windows.Forms.OpenFileDialog -Property @{ 
            InitialDirectory = (New-Object -ComObject Shell.Application).NameSpace('shell:Downloads')
            Filter = 'ISO (*.iso)|*.iso'
            FileName = "Kali-netinstall.iso"
        }
        if ($FileBrowser.ShowDialog() -eq "OK"){
            return $FileBrowser.FileName
        }else{
            return $null
        } 
    } 
    catch {
        Write-Information "Open file dialog failed to lauch - reverting to CLI" -InformationAction Continue
        return Read-Host -Prompt "Src Iso Path [Download from mirror]"
    }
}

function Open-xorriso-zip-dialog
{
    try{
        $FileBrowser = New-Object System.Windows.Forms.OpenFileDialog -Property @{ 
            InitialDirectory = (New-Object -ComObject Shell.Application).NameSpace('shell:Downloads')
            Filter = 'ZIP (*.zip)|*.zip'
            FileName = "xorriso.zip"
        }
        if ($FileBrowser.ShowDialog() -eq "OK"){
            return $FileBrowser.FileName
        }else{
            return $null
        } 
    } 
    catch {
        Write-Information "Open file dialog failed to lauch - reverting to CLI" -InformationAction Continue
        return Read-Host -Prompt "Src zip Path [Download from mirror]"
    }
}

function GetXorriso(
    [Parameter(Mandatory)][string]$WorkingDir,  
    [Parameter()][string]$zipFile=$null, 
    [Parameter()][string]$sourceUrl = "https://gitlab.com/dittyape/vm-setup/-/raw/main/CreateVM/Windows/xorriso.zip?ref_type=heads"
)
{
    # The zip requires the following specified dlls at the specified paths in the zip
    # These dlls can be found in the zips listed on the same line 
    # All of which can be found at https://mirrors.kernel.org/sourceware/cygwin/x86_64/release/:

    # cygwin/cygwin-3.5.1-1.tar.xz - usr/bin/cygwin1.dll
    # libiconv/libiconv2/libiconv2-1.17-1.tar.xz - usr/bin/cygiconv-2.dll
    # zlib/zlib0/zlib0-1.3.1-1.tar.zst - usr/bin/cygz.dll
    
    # Finally the zip needs to contain usr/bin/xorriso.exe
    # which can be found in https://gitlab.com/dittyape/cygwin-geniso/-/raw/main/xorriso-1.5.6.pl02-1.tar.xz
    # If you do not trust my build follow the instructions in 
    # `https://gitlab.com/dittyape/cygwin-geniso/-/blob/main/README.md` to build your own version
    # This may be usefull if xorriso needs to be updated to a newer version.

    $CygwinDir = Join-Path -Path $WorkingDir -ChildPath "cygwin"
    
    [void](New-Item -ItemType Directory -Path $CygwinDir -Force)
    $xorriso = Join-Path -Path $CygwinDir -ChildPath "usr\bin\xorriso.exe"

    if(-not(Test-Path -Path $xorriso)){
        if ([string]::IsNullOrWhiteSpace($zipFile)){
            $zipFile = Join-Path -Path $CygwinDir -ChildPath "xorriso.zip"               
            Write-Information "Starting download of $sourceUrl" -InformationAction Continue
            $ProgressPreference = 'SilentlyContinue'
            Invoke-WebRequest  -Uri $sourceUrl -OutFile $zipFile
            $ProgressPreference = 'Continue'
            Write-Information "Download of $sourceUrl complete" -InformationAction Continue
        }
        Expand-Archive -LiteralPath $zipFile -DestinationPath $CygwinDir
    }
    return $xorriso
}

class KaliDownloader{
    hidden [string]$WorkingDir
    hidden [string]$mirror
    hidden $sha256record
    [string]$srcIsoPath
    [string]$version
    [boolean]$latest

    KaliDownloader($WorkingDir, $srcIsoPath=$null, $mirror= "https://cdimage.kali.org/current/"){
        $this.WorkingDir = $WorkingDir
        New-Item -ItemType Directory -Path $this.WorkingDir -Force
        $this.mirror = $mirror
        $this.sha256record=$this.GetShaRecord()
        $this.version = "Kali"
        $this.latest = $false
        
        if ([string]::IsNullOrWhiteSpace($srcIsoPath))
        {
            $this.srcIsoPath = $null
            $this.GetLatestkali()
        }else{
            $this.srcIsoPath = $srcIsoPath
            if(Test-Path -Path $this.srcIsoPath){ 
                $this.CheckLatestkali()
                if($false -eq $this.latest){
                    Write-Information "Src Iso does not match lateat version's sha256" -InformationAction Continue
                }
            }else{
                Write-Information "Src Iso not found downloading" -InformationAction Continue
                $this.GetLatestkali()
            }
        }
    }

    [string] hidden DownloadKaliFile([string] $file){
        <#
        .SYNOPSIS
        Downloads a file to the kali download folder.
        #>
        $url = -join($this.mirror, $file)
        Write-Information "Starting download of $url" -InformationAction Continue
        $outputFile = Join-Path -Path $this.WorkingDir -ChildPath $file
        $ProgressPreference = 'SilentlyContinue'
        Invoke-WebRequest -Uri $url -OutFile $outputFile
        $ProgressPreference = 'Continue'
        Write-Information "Download of $url complete" -InformationAction Continue
        return $outputFile
    }

    [array[]] hidden GetShaRecord(){
        <#
        .SYNOPSIS
        Downloads the sha256sums
        Uses the sha256sums to build latest file name.
        Returned values: [sha256,filename,version] or $null
        #>
        $SHA256SUMSPath = $this.DownloadKaliFile("SHA256SUMS")
        $netinstmatches = Select-String -path $SHA256SUMSPath -Pattern '^(?<sha256>[0-9a-fA-F]{64})\s\s(?<filename>kali-linux-(?<version>\d\d\d\d\.\d)-installer-netinst-amd64\.iso)$'
        if($null -eq $netinstmatches){
            #Write-Information "Bad SHA256SUMS file" -InformationAction Continue
            return $null
        }
        $record = New-Object PsObject -Property @{ 
            sha256 = $netinstmatches.Matches[0].groups["sha256"].value;
            filename = $netinstmatches.Matches[0].groups["filename"].value;
            version = $netinstmatches.Matches[0].groups["version"].value;
        }
        return $record
    }
    
    [void] hidden CheckLatestkali(){
    <#
    .SYNOPSIS
    Downloads the sha256sums
    Uses the sha256sums to build latest file name.
    Checks downloaded version matched hash, (if pre existing file matches hash does not redownload)
    
    Sets [boolean]latest.
    #>
        $this.latest = $false
        if($null -eq $this.sha256record){
           Write-Information "Bad SHA256SUMS file" -InformationAction Continue
           if(Test-Path -Path $this.srcIsoPath){ 
                return # return as may be doing iso gen offline with provided ISO
           }else{
                exit
           } 
        }
        if(Test-Path -Path $this.srcIsoPath){ 
            # We have iso predownloaded.
            # Check the hash if invalid redownload
            #  Else return
            if((Get-FileHash $this.srcIsoPath -Algorithm SHA256).Hash -eq $this.sha256record.groups["sha256"].value){ 
                $this.latest = $true
            }
        }
        Write-Information "kali ISO SHA256 mismatch, either wrong edition or outdated" -InformationAction Continue
    }  
    [void] GetLatestkali(){
    <#
    .SYNOPSIS
    Downloads the sha256sums
    Uses the sha256sums to build latest file name.
    Checks downloaded version matched hash, (if pre existing file matches hash does not redownload)
    
    Sets [boolean]latest and [string]version
    #>
         
        if($null -eq $this.sha256record){
           Write-Information "Bad SHA256SUMS file" -InformationAction Continue
           exit 
        }
         if ([string]::IsNullOrWhiteSpace($this.srcIsoPath))
        {
            $this.srcIsoPath = Join-Path -Path $this.WorkingDir -ChildPath $this.sha256record.filename
        }
        $curr_version = -join("Kali ", $this.sha256record.version)
        if(Test-Path -Path $this.srcIsoPath){ 
            # We have iso predownloaded.
            # Check the hash if invalid redownload
            #  Else return
            if((Get-FileHash $this.srcIsoPath -Algorithm SHA256).Hash -eq $this.sha256record.sha256){ 
                $this.version = $curr_version
                $this.latest = $true
                Write-Information "Cached copy is still upto-date skipping download" -InformationAction Continue
                return
            }
        }
        $this.DownloadKaliFile($this.sha256record.filename)
        if((Get-FileHash $this.srcIsoPath -Algorithm SHA256).Hash -eq $this.sha256record.sha256){ 
            $this.version = $curr_version
            $this.latest = $true
            return
        }else{
            Write-Information "kali ISO SHA256 mismatch" -InformationAction Continue
            exit
        }
    }
}

class KaliPatcher{
    hidden [string] $WorkingDir
    hidden [string] $xorriso
    hidden [string] $srcIsoPath
    [string] $destIsoPath
    [string] $preseed=@'
################################
# Custom preseed configuration #
################################
## User account
#d-i passwd/user-fullname string [User's Name(can include spaces)]
#d-i passwd/username string [User name no space]
## Normal user's password, either in clear text
#d-i passwd/user-password password [Plain Pass]
#d-i passwd/user-password-again password [Plain Pass]
## or encrypted using a crypt(3) hash.
#d-i passwd/user-password-crypted password [crypt(3) hash]
#d-i passwd/user-fullname string kaliuser
#d-i passwd/username string kaliuser
## Normal user's password, either in clear text
#d-i passwd/user-password password toor
#d-i passwd/user-password-again password toor
## or encrypted using a crypt(3) hash.
#d-i passwd/user-password-crypted password [crypt(3) hash]

# Network
d-i netcfg/get_hostname string kali
d-i netcfg/get_domain string unassigned-domain
d-i netcfg/choose_interface select eth0
d-i netcfg/dhcp_timeout string 60

# Locale
d-i debian-installer/locale string en_GB.UTF-8
d-i console-keymaps-at/keymap select gb
d-i keyboard-configuration/xkb-keymap select gb

# Timezone
d-i clock-setup/utc boolean true
d-i time/zone string Europe/London

# Don't ask for proxy settings
d-i mirror/http/proxy string

# Partitioning

# The presently available methods are:
# - regular: use the usual partition types for your architecture
# - lvm:     use LVM to partition the disk
# - crypto:  use LVM within an encrypted partition
d-i partman-auto/method string lvm

# You can define the amount of space that will be used for the LVM volume
# group. It can either be a size with its unit (eg. 20 GB), a percentage of
# free space or the 'max' keyword.
d-i partman-auto-lvm/guided_size string max

# You can choose one of the three predefined partitioning recipes:
# - atomic: all files in one partition
# - home:   separate /home partition
# - multi:  separate /home, /var, and /tmp partitions
d-i partman-auto/choose_recipe select atomic

d-i partman-basicfilesystems/no_swap boolean false
d-i partman-auto-lvm/new_vg_name string kali-vg


# This makes partman automatically partition without confirmation, provided
# that you told it what to do using one of the methods above.
d-i partman-partitioning/confirm_write_new_label boolean true
d-i partman/choose_partition select finish
d-i partman/confirm boolean true
d-i partman/confirm_nooverwrite boolean true

d-i partman-lvm/confirm_nooverwrite boolean true

# Packages
tasksel tasksel/first multiselect standard
d-i pkgsel/include string \
curl git kali-linux-core kali-desktop-xfce python3 git

# Grub
d-i grub-installer/only_debian boolean true
d-i grub-installer/with_other_os boolean false
d-i grub-installer/bootdev string /dev/sda

# Automatically reboot after installation
d-i finish-install/reboot_in_progress note

# Eject media after installation
d-i cdrom-detect/eject boolean true
'@
    [string] $postInstall=@'
# Get installer user
installeruser=$(ls /home)
# Setup desktop
su $installeruser --command "git clone https://gitlab.com/dittyape/vm-setup.git ~/Desktop/vm-setup"
# Setup autologin
sed -i.bak "s/#autologin-user=/autologin-user=$installeruser/g" /etc/lightdm/lightdm.conf
# Setup Auto vm-setup on login
su $installeruser --command "mkdir -p ~/.config/autostart/"
su $installeruser --command "cat >> ~/.config/autostart/FirstLaunch.desktop" << EOF 
[Desktop Entry]
Encoding=UTF-8
Version=0.9.4
Type=Application
Name=FirstLaunch
Comment=calls vm-setup/setup.sh, and removes this FirstLaunch script
Exec=/usr/share/kali-menu/exec-in-shell "echo \"STARTING FIRST RUN SETUP\"; cd ~/Desktop/vm-setup/RunInVM/Kali/;bash ./setup.sh && rm ~/.config/autostart/FirstLaunch.desktop && echo \"SETUP COMPLETE :)\" && read -s -k \"?Press any key to continue\" && exit; echo \"SETUP ERRORED :(\""
OnlyShowIn=XFCE;
RunHook=0
StartupNotify=false
Terminal=true
Hidden=false
EOF
'@
    KaliPatcher($WorkingDir, $xorriso, $srcIsoPath, $preseed=$null, $postInstall=$null){
        $this.WorkingDir = $WorkingDir
        New-Item -ItemType Directory -Path $this.WorkingDir -Force
        if ( $null -ne $preseed) {
            $this.preseed = $preseed
        }
        if ( $null -ne $postInstall) {
            $this.postInstall = $postInstall
        }
        $this.srcIsoPath = $srcIsoPath
        $this.destIsoPath = Join-Path -Path $this.WorkingDir -ChildPath "kali-modified.iso"
        $this.xorriso = $xorriso
    }



    [void] hidden CopyDirectories([string] $source, [string] $destination){
        # https://stackoverflow.com/questions/11424582/powershell-copy-item-all-files-from-harddrive-to-another-harddrive
        try {
            Get-ChildItem -Path $source -Recurse -Force |
                Where-Object { $_.psIsContainer } |
                ForEach-Object { $_.FullName -replace [regex]::Escape($source), $destination } |
                ForEach-Object { $null = New-Item -ItemType Container -Path $_ }
            Get-ChildItem -Path $source -Recurse -Force |
               Where-Object { -not $_.psIsContainer } |
               Copy-Item -Force -Destination { $_.FullName -replace [regex]::Escape($source), $destination }
        }
        catch {
            Write-Host "$_"
        }
    }

    [void] hidden SetReadOnly([string] $folderPath, [string] $setRoAttributeTo )
    {
        #BUG: method only removes ATTRIB flag
        #BUG: native-powershell variant fails so using CMD variant

        #-r==remove read only; /s==recursive
        attrib -r (-join($folderPath,'\*')) /s

        ## The following fails to remove flag so using CMD variant above to remove readonly flag
        #Get-ChildItem -Recurse -Path $folderPath | foreach {
        #    Write-Information "$_.FullName $setRoAttributeTo" -InformationAction Continue
        #    Set-ItemProperty -Path $_.FullName -Name IsReadOnly -Value $setRoAttributeTo
        #    Get-ItemProperty -Path $_.FullName -Name IsReadOnly
        #}
    }

    [void] hidden ExtractIso([string] $destinationFolder){
        $DiskImage = Mount-DiskImage -ImagePath $this.srcIsoPath -PassThru
        $this.CopyDirectories((-join((Get-Volume -DiskImage $DiskImage).DriveLetter,':\')), (-join($destinationFolder,'\')))
        $this.SetReadOnly($destinationFolder,$false)
        Dismount-DiskImage -DevicePath $DiskImage.DevicePath
    }

    [void] hidden UpdateBootFile([string] $CfgFile){
        # Using default over kali as then kali.postinstall will auto run with no issue.
        $content = [System.IO.File]::ReadAllText($CfgFile).Replace(
            "preseed/file=/cdrom/simple-cdd/default.preseed",
            "auto=true preseed/file=/cdrom/simple-cdd/default.preseed"
        ).Replace(
            "ontimeout /install.amd/vmlinuz desktop=xfce vga=788 initrd=/install.amd/gtk/initrd.gz speakup.synth=soft --- quiet",
            "ontimeout /install.amd/vmlinuz auto=true preseed/file=/cdrom/simple-cdd/default.preseed simple-cdd/profiles=kali desktop=xfce vga=788 initrd=/install.amd/gtk/initrd.gz --- quiet"
        ).Replace(
            "menu autoboot Press a key, otherwise speech synthesis will be started in # second{,s}...",
            "menu autoboot Press a key, otherwise install will be started in # second{,s}..."
        )
        [System.IO.File]::WriteAllText($CfgFile, $content)
    }

    [void] hidden UpdateBootCFG([string] $isoFolder){
        # Update grubmenu items
        $GrubCfgFile = Join-Path -Path $isoFolder -ChildPath ".\boot\grub\grub.cfg"
        $this.UpdateBootFile($GrubCfgFile)
        # Update BIOS/isolinux items
        $isolinux = Join-Path -Path $isoFolder -ChildPath ".\isolinux\"
        Get-ChildItem $isolinux -Filter *.cfg | Foreach-Object { $this.UpdateBootFile($_.FullName) }
    }

    [void] UpdateKaliFile([string] $isoFolder, [string] $kaliFile, [string] $content){
    <#
    .Synopsis
    Updates specified kali file in the isofolder.

    .Description
    Updates specified kali file in the isofolder.

    .Parameter isoFolder
    Root of the extracted iso.

    .Parameter kaliFile
    File in Iso to update.

    .Parameter content
    Content of the file to save.

    .OUTPUTS
    None. 

    .Example
    Update-KaliFile $TempDir ".\simple-cdd\default.preseed" $preseedContent
    #>
        $file = Join-Path -Path $isoFolder -ChildPath $kaliFile
        $additions=$content.Replace("`r`n","`n") # Change to linux line endings
        Add-Content -Path $file -Value $additions -NoNewline
    }

    [void] MakeIso([string] $srcFolder){
        Push-Location $srcFolder
        # dd if="$orig_iso" bs=1 count=432 of="syslinux/usr/lib/ISOLINUX/isohdpfx.bin"
        #& $xorriso -as mkisofs -r -V 'Kali Linux amd64 n' -o $destIso -J -joliet-long -cache-inodes -isohybrid-mbr syslinux/usr/lib/ISOLINUX/isohdpfx.bin -b isolinux/isolinux.bin -c isolinux/boot.cat -boot-load-size 4 -boot-info-table -no-emul-boot -eltorito-alt-boot -e boot/grub/efi.img -no-emul-boot -isohybrid-gpt-basdat -isohybrid-apm-hfsplus .
        & $this.xorriso -as mkisofs -r -V 'Kali Linux amd64 n' -o $this.destIsoPath -J -joliet-long -cache-inodes -b isolinux/isolinux.bin -c isolinux/boot.cat -boot-load-size 4 -boot-info-table -no-emul-boot -eltorito-alt-boot -e boot/grub/efi.img -no-emul-boot -isohybrid-gpt-basdat -isohybrid-apm-hfsplus .
        Pop-Location
    }

    [void] PatchIso()
    {
        # Create Temp dir
        $TempDir = New-Item -ItemType Directory -Path (Join-Path -Path $this.WorkingDir -ChildPath "isoWorkingDir")
        try {
            $this.ExtractIso($TempDir)
            $this.UpdateBootCFG($TempDir)
            $this.UpdateKaliFile($TempDir, ".\simple-cdd\default.preseed", $this.preseed)
            $this.UpdateKaliFile($TempDir, ".\simple-cdd\default.postinst", $this.postinstall)
            $this.MakeIso($TempDir)
        }
        finally {
            # clean Temp dir
            Remove-Item -Path $TempDir -Recurse -Force
        }
    }

    [void] SetUsername([string] $username){
        if (-not [string]::IsNullOrWhiteSpace($username))
        {
                $this.preseed = $this.preseed.Replace(
                    "#d-i passwd/user-fullname string kaliuser",
                    (-join("d-i passwd/user-fullname string ", $username))
                ).Replace(
                    "#d-i passwd/username string kaliuser",
                    (-join("d-i passwd/username string ", $username))
                )
        }
    }
    
    [void] SetPassword([string] $password){
    # TODO: change to secure string and store as encripted linux string.
        if (-not [string]::IsNullOrWhiteSpace($password))
        {
            $this.preseed = $this.preseed.Replace(
                "#d-i passwd/user-password password toor",
                (-join("d-i passwd/user-password password ", $password))
            ).Replace(
                "#d-i passwd/user-password-again password toor",
                (-join("d-i passwd/user-password-again password ", $password))
            )
        }
    }
}

class Hypervisor{
    [array[]] GetVMPath($kaliVersion){
        throw "not implemented"
    }
    
    [string] CreateDisk([string] $VMpath, [string] $VMname){
        throw "not implemented"
    }
    
    [void] Create([string] $VMpath, [string] $VMname, [string] $VMiso, [string] $VMdiskPath) {
        throw "not implemented"
    }
    
    [void] Start([string] $VMpath, [string] $VMname) {
        throw "not implemented"
    }
    
    [void] CreateStart([string] $VMiso, [string] $kaliVersion) {
        throw "not implemented"
    }
}

class VMware: Hypervisor{
    [string] $VMwarePro;
    [string] $vmware_vdiskmanager;
    [string] $vmrun;

    VMware(){
        $this.VMwarePro = (Join-Path -Path ([Environment]::GetEnvironmentVariable("ProgramFiles(x86)")) -ChildPath "VMware\VMware Workstation");
        $this.vmware_vdiskmanager = (Join-Path -Path $this.VMwarePro -ChildPath "vmware-vdiskmanager.exe");
        $this.vmrun = (Join-Path -Path $this.VMwarePro -ChildPath "vmrun.exe");
    }

    [array[]] GetVMPath($kaliVersion){
        $VMname = Read-Host -Prompt "VM Name [$kaliVersion]"
        $VMpath = $null
        do 
        {
            if ([string]::IsNullOrWhiteSpace($VMname))
            {
                $VMname = $kaliVersion
            }
            $VMdirs = Join-Path -Path ([Environment]::GetFolderPath("MyDocuments")) -ChildPath "Virtual Machines"
            $VMpath = Join-Path -Path  $VMDirs -ChildPath $VMname
            if(Test-Path -Path $VMpath){
                $VMname = Read-Host -Prompt "VM already exists with that name! VM Name [$kaliVersion]"
            }  
        } while(Test-Path -Path $VMpath)
        $VMpath = New-Item -ItemType Directory -Path $VMpath -Force
        return $VMpath, $VMname
    }
    
    [string] CreateDisk([string] $VMpath, [string] $VMname){
    <#
    .SYNOPSIS
    Creates a New VMware Disk image
    Prams:
    $diskSize = Size of Disk in GB
    #>
        # Create Disk
        ## -c Creates a local v disk
        ## -a Specifies the disk adapter type
        ## -s size of disk
        ## -t Specifies the virtual disk type.
        ###    0 - create a growable virtual disk contained in a single file (monolithic sparse)
        ###    1 - create a growable virtual disk split into 2GB files (split sparse)
        ###    2 - create a preallocated virtual disk contained in a single file (monolithic flat)
        ###    3 - create a preallocated virtual disk split into 2GB files (split flat).
        ###    4 - create a preallocated virtual disk compatible with ESX server (VMFS flat).
        ###    5 - create a compressed disk optimized for streaming
        $size = "100GB" 
        $diskName = -Join($VMname, ".vmdk")
        $diskPath = Join-Path -Path $VMpath -ChildPath $diskName
        Write-Information (& $this.vmware_vdiskmanager -c -a lsilogic -s $size -t 1 $diskPath) -InformationAction Continue
        return $diskPath
    }
    
    [void] Create([string] $VMpath, [string] $VMname, [string] $VMiso, [string] $VMdiskPath) {
        $templateName = -Join($VMname, ".vmx")
        $templatePath = Join-Path -Path $VMpath -ChildPath $templateName
        $templateContent=@"
.encoding = "windows-1252"
config.version = "8"
virtualHW.version = "21"
pciBridge0.present = "TRUE"
pciBridge4.present = "TRUE"
pciBridge4.virtualDev = "pcieRootPort"
pciBridge4.functions = "8"
pciBridge5.present = "TRUE"
pciBridge5.virtualDev = "pcieRootPort"
pciBridge5.functions = "8"
pciBridge6.present = "TRUE"
pciBridge6.virtualDev = "pcieRootPort"
pciBridge6.functions = "8"
pciBridge7.present = "TRUE"
pciBridge7.virtualDev = "pcieRootPort"
pciBridge7.functions = "8"
vmci0.present = "TRUE"
hpet0.present = "TRUE"
nvram = "$VMname.nvram"
virtualHW.productCompatibility = "hosted"
powerType.powerOff = "soft"
powerType.powerOn = "soft"
powerType.suspend = "soft"
powerType.reset = "soft"
displayName = "$VMname"
usb.vbluetooth.startConnected = "FALSE"
guestOS = "debian12-64"
tools.syncTime = "FALSE"
sound.autoDetect = "TRUE"
sound.fileName = "-1"
sound.present = "TRUE"
numvcpus = "8"
cpuid.coresPerSocket = "1"
vcpu.hotadd = "TRUE"
memsize = "16384"
scsi0.virtualDev = "lsilogic"
scsi0.present = "TRUE"
scsi0:0.fileName = "$VMname.vmdk"
scsi0:0.present = "TRUE"
ide1:0.deviceType = "cdrom-image"
ide1:0.fileName = "$VMiso"
ide1:0.present = "TRUE"
usb.present = "TRUE"
ehci.present = "TRUE"
usb_xhci.present = "TRUE"
ethernet0.connectionType = "nat"
ethernet0.addressType = "generated"
ethernet0.virtualDev = "e1000"
ethernet0.present = "TRUE"
extendedConfigFile = "$VMname.vmxf"
floppy0.present = "FALSE"    
"@
        Set-Content -Path $templatePath -Value $templateContent
        #TODO: Add Optional fix for slow vm on Recent Intel CPUs 
    }
    
    [void] Start([string] $VMpath, [string] $VMname) {
        $templateName = -Join($VMname, ".vmx")
        $templatePath = Join-Path -Path $VMpath -ChildPath $templateName
        & $this.vmrun start $templatePath
    }
    
    [void] CreateStart([string] $VMiso, [string] $kaliVersion) {
        $VMpath, $VMname = $this.GetVMPath($kaliVersion)
        $diskPath = $this.Createdisk($VMpath,$VMname)
        $this.Create($VMpath, $VMname, $VMiso, $diskPath)
        $this.Start($VMpath, $VMname)
    }
}


# Select and initialize Hypervisor
$hypervisorType = Read-Choice 'Hypervisor' '&vmware','&oracle virtualbox','&hyperv','&Other/None' '&vmware' 'Which hypervisor?'
switch($hypervisorType) {
    '&vmware' {
        $hypervisor = [VMware]::new()
    }
    '&oracle virtualbox' {
    }
    '&hyperv' {
    }
    '&Other/None' {
    }
}




# Get working Dir
$downloads_kali = Join-Path -Path ((New-Object -ComObject Shell.Application).NameSpace('shell:Downloads').Self.Path) -ChildPath "kali"
$WorkingDir = Read-Host -Prompt "Working Dir [$downloads_kali]"
if ([string]::IsNullOrWhiteSpace($WorkingDir))
{
    $WorkingDir = $downloads_kali
}

#region Kali download methods


# Get local iso path
$kalimirror = Read-Host -Prompt "Kali mirror [https://cdimage.kali.org/current/]"
if ([string]::IsNullOrWhiteSpace($kalimirror))
{
    $kalimirror = "https://cdimage.kali.org/current/"
}

$srcIsoPath=$null
$download = Read-Choice 'Pre-Downloaded' '&yes','&no' '&no' 'Supply pre-downloaded ISO?'
switch($download) {
    '&yes' {
        $srcIsoPath=Open-kali-iso-dialog
    }
}

$kalidownloader = [KaliDownloader]::new($WorkingDir, $srcIsoPath, $kalimirror)
if($kalidownloader.lateat -eq $false){
    $download = Read-Choice 'UpdateIso' '&yes','&no' '&yes' 'Supplied ISO does not match latest, download latest ISO?'
    switch($download) {
        '&yes' {
            $kalidownloader.GetLatestkali()
        }
    }    
}

$srcIsoPath=$null
$download = Read-Choice 'Pre-Downloaded' '&yes','&no' '&no' 'Supply pre-downloaded xorriso zip?'
switch($download) {
    '&yes' {
        $srcxorrisoPath=Open-xorriso-zip-dialog
    }
}

$xorriso = GetXorriso $WorkingDir $srcxorrisoPath "https://gitlab.com/dittyape/vm-setup/-/raw/main/CreateVM/Windows/xorriso.zip?ref_type=heads"

# Patch kali
$kalipatcher = [KaliPatcher]::new($WorkingDir, $xorriso, $kalidownloader.srcIsoPath, $null, $null)
$kalipatcher.SetUsername((Read-Host -Prompt "Username [ask during install]"))
$kalipatcher.SetPassword((Read-Host -Prompt "Password [ask during install]"))
# Patch kali
$kalipatcher.PatchIso()

# If hypervisor selected start VM
if($null -ne $hypervisor){
    $hypervisor.CreateStart($kalipatcher.destIsoPath, $kalidownloader.version)
}