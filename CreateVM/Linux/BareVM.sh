#! /bin/env bash

#IMPORTANT: DONOT call with `.` use `bash` instead.
#Stop on error
	set -euo pipefail

function checkCMD(){
    command -v $1
    if [ "$?" -ne 0 ]; then
        echo "$1: not found please install" 1>&2
        echo "Apt requirements: sudo apt install xorriso curl" 1>&2
        exit
    fi
}

