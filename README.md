# VM setup scrips

This repo contains the public aspects of my vm setup scripts.

These scripts are used to create various VMs with:
* KaliVM:
    * tooling I regularly use, either insalled via apt or the tools directory stored on the desktop
    * the current user set to auto login.
    * the vm set not go into standby/sleep after inactivity.
    * When installing check the last part of output. As the install should stop if there are any errors.
* WindowsVM
    * tooling I regularly use
    * When installing check all of the output for errors.

## Create VM
See for how to create a vm automaticly and start it - [CreateVM/Windows/Readme.md](CreateVM/Windows/Readme.md)
### Kali on Windows quick command
```
powershell -exec bypass -c "iwr('https://gitlab.com/dittyape/vm-setup/-/raw/main/CreateVM/Windows/kali_on_windows.ps1?ref_type=heads')|iex"
```

## Post VM creation Steps
### Kali
If you created the vm via the Create VM script follow the instructions on the desktop.

Otherwise if you created a minimal install of kali(with xfce desktop), and wish to just install my tools ontop (NOT RECOMMENED) follow [RunInVM/Kali/Readme.md](RunInVM/Kali/Readme.md)

### Windows
```
powershell -exec bypass -c "iwr('https://gitlab.com/dittyape/vm-setup/-/raw/main/RunInVM/windows_setup.ps1?ref_type=heads')|iex"
```